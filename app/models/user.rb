class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :histories

  # campo 'name' como obligatorio
  validates :name, presence: true

  # campo 'username' como único y obligatorio
  validates :username, uniqueness: true
  validates :username, presence: true
end
